# Purpose

It's backend only exposing GraphQL.

I am experimenting with:

- Micronaut's boot/run performance
- Exposed for database access
- GraphQL on the JVM
- How I can combine Exposed with GraphQL DataFetchers to only retrieve what has been asked for (entity level).  There is User and UserProfile.  The User may or may not have a UserProfile.
- TODO Query only the fields asked for using the Exposed schema and the "by" property on the DAO.


# Repo

git clone git@bitbucket.org:djsmart/micronaut-kotlin-exposed-graphql.git

# Basic Requirements

You could just get the Oracle Java JDK 8.

Better to use SDKMAN to get the Amazon JDK 11:

```bash
sdk use java 11.0.5-amzn
```

Assuming you don't have at least JDK 9, this will install amazon's tuned and open-source java dev kit and runtime.

# Running the Server

To run it, just use:

```bash
./gradlew run
```

> To run it in IntelliJ you need to turn on "Annotation Processors" for the project.  This is because Micronaut, unlike Spring, trys to NEVER use reflection (a performance killer).  It generates DI bindings etc at compile time.  So IntelliJ just needs to know that it needs to work with that.

# Use

The app builds the database schema into H2, which is an in-memory database server.

Certain sample data is loaded

The GraphQL console is exposed with a sample loaded at:

http://localhost:8080/graphiql?query=query%20%7B%0A%20%20user(uid%3A%20%22usr_UEDFWqlPkR49%22)%20%7B%0A%20%20%20%20uid%0A%20%20%20%20email%0A%20%20%20%20profile%20%7B%0A%20%20%20%20%20%20emergencyPhone%0A%20%20%20%20%20%20emergencyName%0A%20%20%20%20%20%20emergencyRelation%0A%20%20%20%20%20%20phoneMobile%0A%20%20%20%20%20%20phoneHome%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A

This query comes loaded with a user you can load.  Another is "usr_foobar".

# Code bits

UserDataFetcher (configured elsewhere) is the class that performs the User query.  It does a bit of magic optimising the query (ie. whether Profile has been requested).  But that was just mucking around to see how you could do such a thing.  The next step would be only the fields asked for, but I didn't get to that (only relationships).

There's a mutation on the profile too - the aptly named UpdateUserProfileMutation.

The Micronaut DI container is great.  Notice the class DatabaseConfig.  It's the @Factory for the @Singleton function that generates (once and only once) the database connection pool instance.

There's a controller sample to call HelloController.   Just hit /hello/Fred and it maps the last part as "name".  Just tinkering to see how things work.  But it injects a service, which is of course a singleton, which itself has the DI container inject the database, and it performs everything in a transaction.

The Exposed library's transaction {} construct is interesting.  Inside it you can specify ACID controls, logging, etc.  Just search for various places where "transaction" appears.

Under the covers this thing is using the non-blocking IO server - Netty.  Notice the controller returns a Maybe<Something>.  You can return streams too but I didn't get to that.

BTW: Look for @Scheduled...  It's an annotation that can turn a method into a scheduled task.

# Produce Grails WAR

grails -Dgrails.env=dev war

# Deploy to Cloud Foundry

cf push kt-hello-world -p build/libs/kt-hello-world-0.1-all.jar

# Stop and Delete the App

cf stop kt-hello-world

cf delete -r kt-hello-world