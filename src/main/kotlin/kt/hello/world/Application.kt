package kt.hello.world

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("kt.hello.world")
                .mainClass(Application.javaClass)
                .start()
    }
}