package kt.hello.world

import io.micronaut.context.annotation.Factory
import kt.hello.world.dto.UserData
import kt.hello.world.schema.UserIntroViewed
import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.UserStatus
import kt.hello.world.schema.Users
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton
import javax.sql.DataSource

@Factory
class DatabaseConfig(@Inject val dataSource: DataSource) {

    companion object {
        private val LOG = LoggerFactory.getLogger(DatabaseConfig::class.java)
        private var creationTimes = 0
    }

    @Singleton
    fun database(): Database {
        creationTimes++
        LOG.info("Creating database $creationTimes time(s)")
        val database: Database = Database.connect(dataSource)

        LOG.info("Database ${database.url}")

        transaction(database) {
            SchemaUtils.create(Users, UserProfiles)

            val users = listOf(
                // Users.uid eq "usr_foobar"
                UserData(uid = "usr_UEDFWqlPkR49", email = "owner-93mt21zk@mail.s1tj5z.staging.sitesupervisor.com", firstName = "Amalia", lastName = "Heaney", status = UserStatus.ACTIVATED, introViewed = UserIntroViewed.INTRO_NOT_VIEWED),
                UserData(uid = "usr_foobar", email = "bazbing@boo.com", firstName = "MockUser", lastName = "FakeData", status = UserStatus.DEACTIVATED, introViewed = UserIntroViewed.INTRO_VIEWED),
                UserData(uid = "usr_XYZ123", email = "xyz123@boo.com", firstName = "Random1", lastName = "FakeData", status = UserStatus.ACTIVATED, introViewed = UserIntroViewed.INTRO_NOT_VIEWED),
                UserData(uid = "usr_NNP345", email = "nnp345@boo.com", firstName = "Random2", lastName = "FakeData", status = UserStatus.DEACTIVATED, introViewed = UserIntroViewed.INTRO_VIEWED),
                UserData(uid = "usr_2234SDFSCB", email = "2234sdfscb@boo.com", firstName = "NumberMan", lastName = "FakeData", status = UserStatus.ACTIVATED, introViewed = UserIntroViewed.INTRO_NOT_VIEWED),
                UserData(uid = "usr_VERYLONGUID1232435", email = "verylonguid1232435@boo.com", firstName = "VeryLong", lastName = "FakeData", status = UserStatus.DEACTIVATED, introViewed = UserIntroViewed.INTRO_VIEWED)
            )

            users.forEach { userData ->
                val idOfUser = Users.insert { usersTable ->
                    usersTable[Users.uid] = userData.uid
                    usersTable[Users.email] = userData.email
                    usersTable[Users.firstName] = userData.firstName
                    usersTable[Users.lastName] = userData.lastName
                    usersTable[Users.status] = userData.status
                    usersTable[Users.introViewed] = userData.introViewed
                } get Users.id

                if(userData.uid != "usr_foobar") {
                    UserProfiles.insert {
                        it[UserProfiles.emergencyName] = "emergencyName"
                        it[UserProfiles.emergencyPhone] = "emergencyPhone"
                        it[UserProfiles.emergencyRelation] = "emergencyRelation"
                        it[UserProfiles.phoneMobile] = "phoneMobile"
                        it[UserProfiles.phoneHome] = "phoneHome"
                        it[UserProfiles.owner] = idOfUser
                    }
                }
            }
        }

        return database
    }

}