package kt.hello.world.controllers

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import io.micronaut.http.multipart.StreamingFileUpload
import io.reactivex.Single
import java.io.File

@Controller("/api/file/v1")
class FileUploadContoller {

    //    @Post("/upload")
    @Post(value = "/upload", consumes = [MediaType.MULTIPART_FORM_DATA])
    fun upload(file: StreamingFileUpload): Single<HttpResponse<String>> {
        println("File received: ${file.filename}")
        val fullPath = "/Users/dsmart/temp/${file.filename}"
        val tempFile = File(fullPath)

        if (tempFile.exists()) {
            return Single.just(HttpResponse.status<String>(HttpStatus.CONFLICT))
        }
//        val tempFile = File.createTempFile("${file.filename}", "", File("/Users/dsmart/temp"))
        val uploadPublisher = file.transferTo(tempFile)
        println("File saved: ${tempFile.absolutePath}")
        return Single.fromPublisher(uploadPublisher)
            .map { success ->
                if (success) {
                    HttpResponse.ok("Uploaded")
                } else {
                    HttpResponse.status<String>(HttpStatus.CONFLICT)
                        .body("Upload Failed")
                }
            }
    }
}