package kt.hello.world.controllers

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Maybe
import kt.hello.world.dto.EventType
import kt.hello.world.dto.UserData
import kt.hello.world.services.SayHelloService
import java.util.*
import javax.inject.Inject

@Controller("/hello")
class HelloController(@Inject val sayHelloService: SayHelloService) {

    @Get("/{name}")
    fun hello(name: String): Maybe<HelloResponse> {
        val helloList: List<UserData> = sayHelloService.allUserEmails()
        return Maybe.just(HelloResponse(
            eventId = UUID.randomUUID(),
            eventType = EventType.UserWasQueried,
            name = name,
            users = helloList
        ))
    }

}