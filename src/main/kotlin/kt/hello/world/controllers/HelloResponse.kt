package kt.hello.world.controllers

import kt.hello.world.dto.Event
import kt.hello.world.dto.EventType
import kt.hello.world.dto.UserData
import java.util.*

data class HelloResponse(
    override val eventId: UUID,
    override val eventType: EventType,
    val name: String,
    val users: List<UserData>
) : Event