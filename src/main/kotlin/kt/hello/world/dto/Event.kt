package kt.hello.world.dto

import java.util.*

interface Event {
    val eventId: UUID
    val eventType: EventType
}