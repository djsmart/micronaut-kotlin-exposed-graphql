package kt.hello.world.dto

data class ProfileUpdate(
    val uid: String,
    val emergencyName: String?,
    val emergencyPhone: String?,
    val emergencyRelation: String?,
    val phoneMobile: String?,
    val phoneHome: String?
)