package kt.hello.world.dto

import kt.hello.world.schema.UserIntroViewed
import kt.hello.world.schema.UserStatus

data class UserData(
    val uid : String,
    val email : String?,
    val firstName: String,
    val lastName: String,
    val status: UserStatus?,
    val introViewed: UserIntroViewed,
    val profile : UserProfileData? = null
)