package kt.hello.world.graphql

import graphql.GraphQL
import graphql.schema.idl.RuntimeWiring
import graphql.schema.idl.SchemaParser
import graphql.schema.idl.TypeDefinitionRegistry
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import io.micronaut.core.io.ResourceResolver
import kt.hello.world.graphql.User.*
import java.io.BufferedReader
import java.io.InputStreamReader
import javax.inject.Singleton
import graphql.schema.idl.SchemaGenerator as SchemaGenerator1

@SuppressWarnings("Duplicates")
@Factory
class GraphQLFactory {
    @Bean
    @Singleton
    fun graphQL(resourceResolver: ResourceResolver, userFetcher: UserFetcher, userProfileFetcher: UserProfileFetcher, userListFetcher: UserListFetcher, userListFetcherByDao: UserListFetcherByDao, updateUserProfileMutation: UpdateUserProfileMutation): GraphQL {
        val schemaParser = SchemaParser()
        val schemaGenerator = SchemaGenerator1()

        // Parse the schema.
        val typeRegistry = TypeDefinitionRegistry();
        typeRegistry.merge(schemaParser.parse(BufferedReader(InputStreamReader(
            resourceResolver.getResourceAsStream("classpath:schema.graphql").get()))))

        // Create the runtime wiring.
        val runtimeWiring = RuntimeWiring.newRuntimeWiring()
            .type("Query") { typeWiring -> typeWiring
                .dataFetcher("user", userFetcher)
                .dataFetcher("users", userListFetcher)
                .dataFetcher("usersByDao", userListFetcherByDao)
                .dataFetcher("userProfile", userProfileFetcher)
            }
            .type("Mutation") { typeWiring -> typeWiring
                .dataFetcher("updateUserProfile", updateUserProfileMutation)
            }
            .build()

        // Create the executable schema.
        val graphQLSchema = schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring)

        // Return the GraphQL bean.
        return GraphQL.newGraphQL(graphQLSchema).build()
    }

}