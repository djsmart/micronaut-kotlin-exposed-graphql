package kt.hello.world.graphql.User

import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import kt.hello.world.dto.UserProfileData
import kt.hello.world.model.CommonFieldNames
import kt.hello.world.model.UserProfileFieldNames
import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.Users
import kt.hello.world.schema.toProfileData
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateStatement
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpdateUserProfileMutation(@Inject val database: Database) : DataFetcher<UserProfileData> {

    companion object {
        private val LOG = LoggerFactory.getLogger(UpdateUserProfileMutation::class.java)
    }

    override fun get(environment: DataFetchingEnvironment): UserProfileData {

        val input = environment.getArgument<Map<String, Any?>>("input")

        LOG.info("Input has ${input.size} fields")

        input.forEach { entry: Map.Entry<String, Any?> ->
            LOG.info("Parameter: ${entry.key} = ${entry.value}")
        }

        return transaction(database) {

            val columns: List<Column<*>> = listOf(Users.id, *UserProfiles.columns.toTypedArray())

            val originalRow =
                Users.leftJoin(UserProfiles)
                    .slice(columns)
                    .select {
                        Users.uid eq input[CommonFieldNames.uid] as String
                    }.limit(1).first()

            val userProfileIdIsNotNull = originalRow[UserProfiles.id] != null

            LOG.info("UserProfiles.id exists is $userProfileIdIsNotNull")
            LOG.info("Updating for Users.id ${originalRow[Users.id]}")

            return@transaction if (input.size > 1) {
                // Fields other than uid have been posted to be updated
                if (userProfileIdIsNotNull) {
                    LOG.info("Updating UserProfile")
                    val updatedCount = UserProfiles.update(limit = 1, where = { UserProfiles.id eq originalRow[UserProfiles.id] }) { it: UpdateStatement ->
                        if (input.containsKey(UserProfileFieldNames.emergencyName))
                            it[emergencyName] = input[UserProfileFieldNames.emergencyName] as String?
                        if (input.containsKey(UserProfileFieldNames.emergencyPhone))
                            it[emergencyPhone] = input[UserProfileFieldNames.emergencyPhone] as String?
                        if (input.containsKey(UserProfileFieldNames.emergencyRelation))
                            it[emergencyRelation] = input[UserProfileFieldNames.emergencyRelation] as String?
                        if (input.containsKey(UserProfileFieldNames.phoneHome))
                            it[phoneHome] = input[UserProfileFieldNames.phoneHome] as String?
                        if (input.containsKey(UserProfileFieldNames.phoneMobile))
                            it[phoneMobile] = input[UserProfileFieldNames.phoneMobile] as String?
                    }
                    LOG.info("${updatedCount} UserProfiles updated")
                    val updatedRow =
                        UserProfiles
                            .slice(*UserProfiles.columns.toTypedArray())
                            .select {
                                UserProfiles.id eq originalRow[UserProfiles.id]
                            }.limit(1).first()
                    updatedRow.toProfileData()
                } else {
                    LOG.info("Creating UserProfile")
                    val profileId = UserProfiles.insert {
                        it[emergencyName] = input[UserProfileFieldNames.emergencyName] as String?
                        it[emergencyPhone] = input[UserProfileFieldNames.emergencyPhone] as String?
                        it[emergencyRelation] = input[UserProfileFieldNames.emergencyRelation] as String?
                        it[phoneHome] = input[UserProfileFieldNames.phoneHome] as String?
                        it[phoneMobile] = input[UserProfileFieldNames.phoneMobile] as String?
                        it[owner] = originalRow[Users.id]
                    } get UserProfiles.id

                    val createdRow =
                        UserProfiles
                            .slice(*UserProfiles.columns.toTypedArray())
                            .select {
                                UserProfiles.id eq profileId
                            }.limit(1).first()
                    createdRow.toProfileData()
                }
            } else {
                // No fields have been posted to update
                LOG.info("No fields to update on UserProfile, but returns latest data from server-side")
                originalRow.toProfileData()
            }
        }

    }

}