package kt.hello.world.graphql.User

import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import graphql.schema.DataFetchingFieldSelectionSet
import kt.hello.world.dto.UserProfileData
import kt.hello.world.dto.UserData
import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.Users
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton
import kt.hello.world.schema.toProfileData


@Singleton
class UserFetcher(@Inject val database: Database) : DataFetcher<UserData> {

    companion object {
        private val LOG = LoggerFactory.getLogger(UserFetcher::class.java)
    }

    override fun get(environment: DataFetchingEnvironment): UserData {
        val uid = environment.getArgument<String>("uid")
        return transaction(database) {

            addLogger(StdOutSqlLogger)

            val fieldSelectionSet: DataFetchingFieldSelectionSet = environment.selectionSet
            println("Requested fields: ${fieldSelectionSet.fields.joinToString(", ", "", "") { "${it.name}(${it.qualifiedName})" }}")

            val profileRequested = fieldSelectionSet.fields.find { it.name == "profile" }?.name != null

            println("Users columns: ${Users.columns.map { column -> column.name }}")
            println("UserProfiles columns: ${UserProfiles.columns.map { column -> column.name }}")

            val tableOrJoin: ColumnSet =
                when (profileRequested) {
                    true -> Users.leftJoin(UserProfiles)
                    else -> Users
                }

            val queryResult: ResultRow =
                tableOrJoin
                    // Could calculate the fields to include from the arguments
                    // .slice(Users.id, Users.uid, Users.email, UserProfiles.id, UserProfiles.phoneMobile, UserProfiles.phoneHome, UserProfiles.emergencyPhone)
                    .slice(*tableOrJoin.columns.toTypedArray())
                    .select {
                        Users.uid eq uid
                    }.limit(1).first()

            val profile: UserProfileData? =
                when (profileRequested) {
                    true -> queryResult.toProfileData()
                    else -> null
                }

            UserData(
                uid = queryResult[Users.uid],
                email = queryResult[Users.email],
                firstName = queryResult[Users.firstName],
                lastName = queryResult[Users.lastName],
                status = queryResult[Users.status],
                introViewed = queryResult[Users.introViewed],
                profile = profile
            )
        }
    }

}