package kt.hello.world.graphql.User

import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import graphql.schema.DataFetchingFieldSelectionSet
import kt.hello.world.dto.UserData
import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.Users
import kt.hello.world.schema.snakeToCamelCase
import kt.hello.world.schema.toUserData
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.reflect.full.memberProperties

@Singleton
class UserListFetcher(@Inject val database: Database) : DataFetcher<List<UserData>> {

    companion object {
        private val LOG = LoggerFactory.getLogger(UserListFetcher::class.java)
    }

    override fun get(environment: DataFetchingEnvironment): List<UserData> {
        val offset = environment.getArgument<Int>("offset")
        val limit = environment.getArgument<Int>("limit")
        return transaction(database) {
            addLogger(StdOutSqlLogger)

            val fieldSelectionSet: DataFetchingFieldSelectionSet = environment.selectionSet
            println("Requested fields: ${fieldSelectionSet.fields.joinToString(", ", "", "") { "${it.qualifiedName}" }}")
            val profileRequested = fieldSelectionSet.fields.find { it.name == "profile" }?.name != null

            println("Users columns: ${Users.columns.map { column -> column.name }}")
            println("UserProfiles columns: ${UserProfiles.columns.map { column -> column.name }}")

            val tableOrJoin: ColumnSet =
                when (profileRequested) {
                    true -> Users.leftJoin(UserProfiles)
                    else -> Users
                }

            val queryColumns = tableOrJoin.columns.toTypedArray()


            val members = UserData::class.memberProperties.map { it.name }
            println("UserData members = ${members.joinToString(", ")}")

            val queryColumnSqlNames = queryColumns.map { it.name.snakeToCamelCase() }
            println("Query Columns as Camel = ${queryColumnSqlNames.joinToString(", ")}")

            val columnNames = queryColumns.map {
                if(Users.tableName == it.table.tableName) {
                    "${it.name}"
                } else {
                    "${it.table.tableName}.${it.name}"
                }
            }.joinToString(separator = ", ")

            println("Query columns: $columnNames")

            val queryResult =
                tableOrJoin
                    .slice(*queryColumns)
                    .selectAll()
                    .limit(limit, offset)

            return@transaction queryResult.map { row: ResultRow ->
                row.toUserData()
            }
        }
    }

}