package kt.hello.world.graphql.User

import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import graphql.schema.DataFetchingFieldSelectionSet
import kt.hello.world.model.User
import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.Users
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserListFetcherByDao(@Inject val database: Database) : DataFetcher<List<User>> {

    companion object {
        private val LOG = LoggerFactory.getLogger(UserListFetcherByDao::class.java)
    }

    override fun get(environment: DataFetchingEnvironment): List<User> {
        val offset = environment.getArgument<Int>("offset")
        val limit = environment.getArgument<Int>("limit")
        return queryForList(environment, limit, offset)
    }

    private fun queryForList(environment: DataFetchingEnvironment, limit: Int, offset: Int): List<User> {
        val result = transaction(database) {
            addLogger(StdOutSqlLogger)

            val fieldSelectionSet: DataFetchingFieldSelectionSet = environment.selectionSet
            println(
                "Requested fields: ${
                    fieldSelectionSet.fields.joinToString(
                        ", ",
                        "",
                        ""
                    ) { "${it.qualifiedName}" }
                }"
            )
            val profileRequested = fieldSelectionSet.fields.find { it.name == "profile" }?.name != null

            println("Users columns: ${Users.columns.map { column -> column.name }}")
            println("UserProfiles columns: ${UserProfiles.columns.map { column -> column.name }}")

            val query = Users.leftJoin(UserProfiles)
                .slice(Users.columns + UserProfiles.columns)
                .selectAll()
                .limit(n = limit, offset = offset)

            // wrapRows is deep, wraps User.profile with the UserProfile values if present, null if id is null
            val users = User.wrapRows(query).toList()

            users.forEach { user: User ->
                val up = user.profile
                val profile: String = if (up == null) {
                    "Profile is null"
                } else {
                    "Profile(id = ${up.id}, mobile = ${up.phoneMobile})"
                }
                println("User(${user.id}) : ${user.firstName} ${user.lastName} : Profile(${profile})")
            }
            // All the data gets displayed if profile is not null.

            //TODO Problem is that the GraphQL-Java library throws an error if UserProfile data is part of the graphql query!
            return@transaction users
        }

//        println("Outside Transaction:")
//        result.forEach { user: User ->
//            val up = user.profile
//            val profile: String = if (up == null) {
//                "Profile is null"
//            } else {
//                "Profile(id = ${up.id}, mobile = ${up.phoneMobile})"
//            }
//            println("User(${user.id}) : ${user.firstName} ${user.lastName} : Profile(${profile})")
//        }
//        println("Outside Transaction: Done")

        return result
    }

}