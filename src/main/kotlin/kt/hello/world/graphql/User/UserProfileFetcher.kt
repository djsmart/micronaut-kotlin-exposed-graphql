package kt.hello.world.graphql.User

import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import kt.hello.world.dto.UserProfileData
import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.Users
import kt.hello.world.schema.toProfileData
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserProfileFetcher (@Inject val database: Database) : DataFetcher<UserProfileData?> {

    companion object {
        private val LOG = LoggerFactory.getLogger(UserProfileFetcher::class.java)
    }

    override fun get(environment: DataFetchingEnvironment): UserProfileData? {
        val uid = environment.getArgument<String>("uid")
        return transaction(database) {
            val complexJoin = Join(
                Users, UserProfiles,
                onColumn = Users.id, otherColumn = UserProfiles.owner,
                joinType = JoinType.RIGHT)

            val queryResult: ResultRow? =
                complexJoin
                    .slice(*UserProfiles.columns.toTypedArray())
                    .select {
                        Users.uid eq uid
                    }
                    .limit(1)
                    .firstOrNull()

            when(queryResult) {
                null -> return@transaction null
                else -> queryResult.toProfileData()
            }
        }
    }
}