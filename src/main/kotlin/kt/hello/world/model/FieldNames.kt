package kt.hello.world.model

import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.UserProfiles.nullable
import org.jetbrains.exposed.sql.Column

object CommonFieldNames {
    val uid = "uid"
}

object UserFieldNames {
    val email = "email"
    val firstName = "firstName"
    val lastName = "lastName"
    val status = "status"
    val introViewed = "introViewed"
}

object UserProfileFieldNames {
    val emergencyName = "emergencyName"
    val emergencyPhone = "emergencyPhone"
    val emergencyRelation = "emergencyRelation"
    val phoneMobile = "phoneMobile"
    val phoneHome = "phoneHome"
}