package kt.hello.world.model

import kt.hello.world.schema.UserIntroViewed
import kt.hello.world.schema.UserProfiles
import kt.hello.world.schema.UserStatus
import kt.hello.world.schema.Users
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class User(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<User>(Users)

    var uid by Users.uid
    var email by Users.email
    var firstName: String by Users.firstName
    var lastName: String by Users.lastName
    var status: UserStatus? by Users.status
    var introViewed: UserIntroViewed by Users.introViewed
    val profile: UserProfile? by UserProfile optionalBackReferencedOn UserProfiles.owner
}
