package kt.hello.world.model

import kt.hello.world.schema.UserProfiles
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class UserProfile(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserProfile>(UserProfiles)

    var emergencyName by UserProfiles.emergencyName
    var emergencyPhone by UserProfiles.emergencyPhone
    var emergencyRelation by UserProfiles.emergencyRelation
    var phoneMobile by UserProfiles.phoneMobile
    var phoneHome by UserProfiles.phoneHome

//    var owner by User referencedOn UserProfiles.owner
}