package kt.hello.world.schema

import kt.hello.world.dto.UserProfileData
import kt.hello.world.dto.UserData
import org.jetbrains.exposed.sql.ResultRow

fun String.snakeToCamelCase() : String {
    return this.toCharArray().foldIndexed(Pair(false, "")) { index: Int, acc: Pair<Boolean, String>, c: Char ->
        if(acc.first) {
            Pair(false, acc.second + c.toUpperCase())
        } else {
            if(c == '_') {
                Pair(true, acc.second)
            } else {
                Pair(false, acc.second + c)
            }
        }
    }.second
}

fun ResultRow.toUserData(): UserData {
    val profileData: UserProfileData? = if(this.hasValue(UserProfiles.id)) {
        this.toProfileData()
    } else {
        null
    }
    return UserData(
        uid = this[Users.uid],
        email = this[Users.email],
        firstName = this[Users.firstName],
        lastName = this[Users.lastName],
        status = this[Users.status],
        introViewed = this[Users.introViewed],
        profile = profileData
    )
}

fun ResultRow.toProfileData(): UserProfileData {
    return UserProfileData(
        emergencyName = this[UserProfiles.emergencyName],
        emergencyPhone = this[UserProfiles.emergencyPhone],
        emergencyRelation = this[UserProfiles.emergencyRelation],
        phoneHome = this[UserProfiles.phoneHome],
        phoneMobile = this[UserProfiles.phoneMobile]
    )
}