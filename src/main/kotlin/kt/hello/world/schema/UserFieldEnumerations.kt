package kt.hello.world.schema

enum class UserSetup {
    NOT_SETUP, SETUP
}

enum class UserStatus {
    DEACTIVATED, ACTIVATED
}

enum class UserIntroViewed {
    INTRO_NOT_VIEWED, INTRO_VIEWED
}
