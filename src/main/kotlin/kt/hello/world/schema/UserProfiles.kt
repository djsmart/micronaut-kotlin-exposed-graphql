package kt.hello.world.schema

import kt.hello.world.schema.Users.uniqueIndex
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption

object UserProfiles : IntIdTable("user_profiles") {

    val emergencyName : Column<String?> = varchar("emergency_name", 255).nullable()
    val emergencyPhone : Column<String?> = varchar("emergency_phone", 255).nullable()
    val emergencyRelation : Column<String?> = varchar("emergency_relation", 255).nullable()
    val phoneMobile : Column<String?> = varchar("phone_mobile", 25).nullable()
    val phoneHome : Column<String?> = varchar("phone_home", 25).nullable()

    val owner = reference(name = "owner_id", foreign = Users, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.NO_ACTION)
}

/*

create table user_profiles
(
	id int unsigned auto_increment
		primary key,
	owner_id int unsigned not null,
	emergency_name varchar(255) null,
	emergency_phone varchar(255) null,
	emergency_relation varchar(255) null,
	phone_mobile varchar(25) null,
	phone_home varchar(25) null,
	qualifications text null,
	profession varchar(255) null,
	red_white_card varchar(255) null,
	red_white_card_link varchar(255) null,
	company_induction date null,
	last_tetanus date null,
	allergies text null,
	tfn varchar(255) null,
	birthday date null,
	super_annuation json null,
	redundancy json null,
	doctor json null,
	profession_license varchar(255) null,
	current_medication text null,
	medical_history text null,
	lsl json null,
	languages text null,
	created_at timestamp null,
	updated_at timestamp null,
	constraint user_profiles_owner_id_foreign
		foreign key (owner_id) references user_users (id)
			on delete cascade
)
collate=utf8mb4_unicode_ci;


 */