package kt.hello.world.schema

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Column

object Users : IntIdTable("user_users") {
    //    val id: Column<Int> = integer("id").autoIncrement().primaryKey()
    val uid: Column<String> = varchar("uid", SchemaConstants.UID_LENGTH).uniqueIndex()
    val email: Column<String?> = varchar("email", 255).nullable()
    val firstName: Column<String> = varchar(name = "first_name", length =255)
    val lastName: Column<String> = varchar(name = "last_name", length =255)
// 	status int unsigned default 0 null,
    val status: Column<UserStatus?> = enumeration(name = "status", klass = UserStatus::class).nullable()
//	setup int unsigned default 0 not null,
//	intro_viewed int unsigned default 0 not null,
    val introViewed: Column<UserIntroViewed> = enumeration(name = "intro_viewed", klass = UserIntroViewed::class)
}

/*
create table if not exists user_users
(
	id int unsigned auto_increment
		primary key,
	uid varchar(50) not null,
	email varchar(255) null,
	new_email varchar(255) null,
	new_email_confirm int unsigned null,
	password varchar(60) not null,
	first_name varchar(255) not null,
	last_name varchar(255) not null,
	description text null,
	type varchar(255) not null,
	language varchar(3) not null,
	timezone varchar(45) not null,
	session_token text null,
	socket_id varchar(25) null,
	remember_token varchar(100) null,
	status int unsigned default 0 null,
	setup int unsigned default 0 not null,
	intro_viewed int unsigned default 0 not null,
	confirm_key varchar(255) null,
	theme varchar(20) default 'dark' not null,
	temp_password varchar(255) null,
	notify_email tinyint(1) default 1 not null,
	notify_sms tinyint(1) default 1 not null,
	langs json null,
	temporary_password tinyint(1) default 0 not null,
	archived tinyint(1) default 0 not null,
	preferences json null,
	deleted_at timestamp null,
	created_at timestamp null,
	updated_at timestamp null,
	is_customer tinyint(1) default 1 not null,
	last_active_at datetime null,
	constraint user_users_uid_unique
		unique (uid)
) collate=utf8mb4_unicode_ci;
 */
