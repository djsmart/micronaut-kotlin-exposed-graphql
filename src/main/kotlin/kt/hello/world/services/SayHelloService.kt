package kt.hello.world.services

import kt.hello.world.dto.UserData

interface SayHelloService {
    fun say(name: String): String
    fun allUserEmails(): List<UserData>
}