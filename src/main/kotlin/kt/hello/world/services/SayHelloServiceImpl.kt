package kt.hello.world.services

import kt.hello.world.dto.UserProfileData
import kt.hello.world.dto.UserData
import kt.hello.world.model.User
import kt.hello.world.model.UserProfile
import kt.hello.world.schema.Users
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.transactions.transactionManager
import org.slf4j.LoggerFactory
import java.sql.Connection
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
//@Requirements(Requires(beans = [DataSource::class]), Requires(property = "datasource.url"))
class SayHelloServiceImpl(@Inject val database: Database) : SayHelloService {

    companion object {
        private val LOG = LoggerFactory.getLogger(SayHelloServiceImpl::class.java)
    }

    override fun say(name: String): String {
        return "Service says Hello $name"
    }

    override fun allUserEmails(): List<UserData> {
//        val database: Database = Database.connect(dataSource)
// Example of setting a different transaction isolation level
//        database.transactionManager.defaultIsolationLevel = Connection.TRANSACTION_READ_UNCOMMITTED
//        val isolationLevel = when(database.transactionManager.defaultIsolationLevel) {
//            Connection.TRANSACTION_SERIALIZABLE -> "Serialisable"
//            Connection.TRANSACTION_READ_UNCOMMITTED -> "Read Uncommitted"
//            Connection.TRANSACTION_NONE -> "None"
//            Connection.TRANSACTION_READ_COMMITTED -> "Read Committed"
//            Connection.TRANSACTION_REPEATABLE_READ -> "Repeatable Read"
//            else -> "Unknown"
//        }
//        println("Transaction isolation level = ${isolationLevel}")

//        transaction(database) {
//            addLogger(StdOutSqlLogger)
        // Using DAO query with parent relationship to User
//            UserProfile
//                .find {
//                    UserProfiles.phoneMobile.isNotNull()
//                }
//                .forEach { profile ->
//                    println("UserProfile(")
//                    println("\tid = ${profile.id}")
//                    println("\temergencyName = ${profile.emergencyName}")
//                    println("\temergencyPhone = ${profile.emergencyPhone}")
//                    println("\temergencyRelation = ${profile.emergencyRelation}")
//                    println("\tphoneMobile = ${profile.phoneMobile}")
//                    println("\tphoneHome = ${profile.phoneHome}")
//                    println("\towner.uid = ${profile.owner.uid})")
//                    println("\towner.email = ${profile.owner.email})")
//                    println(")")
//                }

        // Using SQL DSL syntax
//            val users = Users.innerJoin(UserProfiles)
//                .select {
//                    phoneMobile.isNotNull() and
//                    Users.email.isNotNull()
//                }
//
//            users
//                .forEach { row ->
//                    println("User(")
//                    println("\tid = ${row[Users.id]}")
//                    println("\tuid = ${row[Users.uid]}")
//                    println("\temail = ${row[Users.email]}")
//                    println("\tprofile.id = ${row[UserProfiles.id]}")
//                    println("\tprofile.phoneMobile = ${row[UserProfiles.phoneMobile]}")
//                    println(")")
//                }

        // Using Optional OneToMany relationship from User to UserProfile
        //   relationship query executes when profile attribute is accessed
//            val users = User
//                .find {
        //                    Users.uid eq "usr_foobar"
//                    Users.email.isNotNull()
//                    and
//                        Users.id.greater(EntityID(4000, Users))
//                }


//            users
//                .forEach { user ->
//                    println("User(")
//                    println("\tid = ${user.id}, uid = ${user.uid}, email = ${user.email}")
//                    println("\tprofile.id = ${user.profile?.id}")
//                    println("\tprofile.phoneMobile = ${user.profile?.phoneMobile}")
//                    println(")")
//                }
//
//        }

        return transaction(database) {
            addLogger(StdOutSqlLogger)
            User
                .find {
                    //                    Users.id greater EntityID(id = 4399, table = Users)
                    Users.email.isNotNull()
                }
                .map { user ->
                    return@map UserData(
                        uid = user.uid,
                        email = user.email,
                        firstName = user.firstName,
                        lastName = user.lastName,
                        status = user.status,
                        introViewed = user.introViewed,
                        profile =
                        // In JSON this yields a user object without the profile field at all { ... }
                        // - this is better because in fact there is no profile row
                        // - any client can use a nullable type and detect that there is no profile
                        // - or resolve it to an Option type of Some(profile) or None.
                        user.profile?.let { profile: UserProfile ->
                            UserProfileData(
                                emergencyName = profile.emergencyName,
                                emergencyRelation = profile.emergencyRelation,
                                emergencyPhone = profile.emergencyPhone,
                                phoneHome = profile.phoneHome,
                                phoneMobile = profile.phoneMobile
                            )
                        }
                        // In JSON this yields a user object with { ..., profile = {} }
                        // - calling .let {} on a null object passes the null into the block
                        // - thus the parameter is ProfileData? and the use of profile?.fieldName
//                        user.profile.let { profile ->
//                            // Because every one of its fields are null the resulting JSON object is { }
//                            ProfileData(
//                                emergencyName = profile?.emergencyName,
//                                emergencyRelation = profile?.emergencyRelation,
//                                emergencyPhone = profile?.emergencyPhone,
//                                phoneHome = profile?.phoneHome,
//                                phoneMobile = profile?.phoneMobile
//                            )
//                        }
                    )
                }
        }
    }

//    @Scheduled(fixedDelay = "40s", initialDelay = "5s")
    fun executeEveryTenSeconds() {
        LOG.info("Initially 10 seconds, then every 3 minutes :{}", SimpleDateFormat("dd/M/yyyy hh:mm:ss").format(Date()))
//        val database: Database = Database.connect(dataSource)

        val isolationLevel = when (database.transactionManager.defaultIsolationLevel) {
            Connection.TRANSACTION_SERIALIZABLE -> "Serialisable"
            Connection.TRANSACTION_READ_UNCOMMITTED -> "Read Uncommitted"
            Connection.TRANSACTION_NONE -> "None"
            Connection.TRANSACTION_READ_COMMITTED -> "Read Committed"
            Connection.TRANSACTION_REPEATABLE_READ -> "Repeatable Read"
            else -> "Unknown"
        }
        println("Transaction isolation level = $isolationLevel")

        transaction(database) {

            addLogger(StdOutSqlLogger)

            val users = User
                .find {
                    //                    Users.uid eq "usr_foobar"
//                    Users.uid eq "usr_UEDFWqlPkR49"
                    Users.email.isNotNull()
                }

            users
                .forEach { user ->
                    LOG.info("User(id = ${user.id}, uid = ${user.uid}, email = ${user.email}, profile.id = ${user.profile?.id}, profile.phoneMobile = ${user.profile?.phoneMobile})")
                }
        }
    }

}