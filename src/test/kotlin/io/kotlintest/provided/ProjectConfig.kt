package io.kotlintest.provided

import io.kotest.core.config.AbstractProjectConfig
import io.micronaut.test.extensions.kotest.MicornautKotlinTestExtension

object ProjectConfig : AbstractProjectConfig() {
    override fun listeners() = listOf(MicornautKotlinTestExtension)
    override fun extensions() = listOf(MicornautKotlinTestExtension)
}
