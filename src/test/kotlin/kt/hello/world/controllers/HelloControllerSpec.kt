package kt.hello.world.controllers

import io.micronaut.context.ApplicationContext
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MutableHttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

@MicronautTest
object HelloControllerSpec: Spek ({
    describe("HelloController Suite") {
        var embeddedServer : EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        var client : HttpClient = HttpClient.create(embeddedServer.url)

        it("/hello/Brian responds with name Brian") {
            val request: MutableHttpRequest<Any> = HttpRequest.GET<Any>("/hello/Brian")
            val rsp: HttpResponse<HelloResponse> = client.toBlocking().exchange(request, Argument.of(HelloResponse::class.java))
            assertEquals(rsp.status(), HttpStatus.OK)
            val body = rsp.body()!!
            assertEquals(body.name, "Brian")
            assertEquals(body.users.size, 6)
        }

        it("/hello/William responds with name William") {
            val request: MutableHttpRequest<Any> = HttpRequest.GET<Any>("/hello/William")
            val rsp: HttpResponse<HelloResponse> = client.toBlocking().exchange(request, Argument.of(HelloResponse::class.java))
            assertEquals(rsp.status(), HttpStatus.OK)
            val body = rsp.body()!!
            assertEquals(body.name, "William")
            assertEquals(body.users.size, 6)
        }

        afterGroup {
            client.close()
            embeddedServer.close()
        }
    }
})